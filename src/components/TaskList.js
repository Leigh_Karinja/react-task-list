const React = require('react');
const _ = require('lodash');

/**
 * The TaskList component renders a view for a list
 * of tasks.
 */

const Task = React.createClass({

getDefaultProps: function() {
return {description: 'get pay rise from bbq galore'};
},


displayName: 'Task',
//Describe how to render
render: function() {
  return (<li>{this.props.description + " " + this.props.completed + " " + this.props.id}</li>
  );

}
}
);

const TaskList = React.createClass({


  // Display name for the component (useful for debugging)
  displayName: 'TaskList',
getInitialState: function() {
return {name: 'isnt it'};
},
  // Describe how to render the component
  render: function() {

   const tasks = _.map(this.props.myTasks, task =>
<Task key={task.id} {...task} />);
const onNameChange = (event) => {
this.setState({name: event.target.value});
}
const onButtonClick = (event) => {
this.props.showMessage("Hello, " + this.state.name + "!");
}
    return (
      <div>
      <input type="text" onChange = {onNameChange} value = {this.state.name} />
      <button onClick = {onButtonClick}> clickman </button>
        <ul>
          {tasks}
        </ul>
      </div>
    );
  }
});



// Export the TaskList component
module.exports = TaskList;
